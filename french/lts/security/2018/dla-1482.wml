#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été découverts dans libx11, l’interface du client
de système X Window. Les fonctions XGetFontPath, XListExtensions et XListFonts
sont vulnérables à un contournement par dépassement d’entier lors de réponses
de serveur malveillant. Un tel serveur pourrait aussi envoyer une réponse dans
laquelle la première chaîne déborde, faisant qu’une variable soit réglée à NULL
puis soit libérée, aboutissant à une erreur de segmentation et un déni
de service. La fonction XListExtensions dans ListExt.c interprète une variable
comme signée au lieu de non signée, ayant pour conséquence une écriture hors limites
(jusqu’à 128 octets), aboutissant à un déni de service ou éventuellement à
l’exécution de code à distance.</p>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 2:1.6.2-3+deb8u2.</p>
<p>Nous vous recommandons de mettre à jour vos paquets libx11.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1482.data"
# $Id: $
