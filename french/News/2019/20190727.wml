#use wml::debian::translation-check translation="b061a3eb5a284dce5a149a8505b3121bd7e4c6b6" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Clôture de DebConf 19 à Curitiba et annonce des dates de DebConf 20</define-tag>

<define-tag release_date>2019-07-27</define-tag>
#use wml::debian::news


<p>
Samedi 27 juillet 2019, la conférence annuelle des développeurs et
contributeurs Debian s'est achevée. Avec plus de 380 participants venus de
50 pays différents, et plus de 145 événements combinant communications,
séances de discussion ou sessions spécialisées, ateliers et activités, 
<a href="https://debconf19.debconf.org">DebConf19</a> a été un énorme
succès.
</p>

<p>
La conférence a été précédée par le DebCamp annuel, du 14 au 19 juillet
qui a mis l'accent sur des travaux individuels et des rencontres d'équipe
pour une collaboration directe au développement de Debian et a accueilli
un atelier d'empaquetage de trois jours où de nouveaux contributeurs ont
pu se lancer dans l'empaquetage Debian.
</p>

<p>
La <a href="https://debconf19.debconf.org/news/2019-07-20-open-day/">journée porte ouverte</a>
s'est tenue le 20 juillet, avec plus de 250 participants qui ont pu
apprécier des présentations et des ateliers intéressant un public plus
large, une bourse à l'emploi avec des stands de plusieurs parrains de
DebConf19 et une « install party » Debian.
</p>

<p>
La conférence des développeurs Debian proprement dite a débuté le
21 juillet 2019. Conjointement à des séances plénières telles que les
traditionnelles « brèves du chef du projet », aux présentations éclair,
aux démonstrations en direct et l'annonce de la DebConf de l'an prochain
(<a href="https://wiki.debian.org/DebConf/20">DebConf20</a> à Haïfa, en Israël),
diverses sessions se sont tenues relatives à la sortie récente de
Debian 10 Buster et certaines de ses nouvelles caractéristiques, ainsi
que des nouvelles et innovations sur plusieurs sujets et équipes internes
de Debian, des sessions de discussion (BoF) des équipes de langue, des
portages, de l'infrastructure et de la communauté, ainsi que de nombreux
autres événements d'intérêt concernant Debian et le logiciel libre.
</p>

<p>
Le <a href="https://debconf19.debconf.org/schedule/">programme</a>
a été mis à jour chaque jour avec des activités planifiées et improvisées
introduites par les participants pendant toute la durée de la conférence.
</p>

<p>
Pour tous ceux qui n'ont pu participer à la DebConf, la plupart des
communications et des sessions ont été enregistrées pour une diffusion
en direct et les vidéos rendues disponibles sur le
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2019/DebConf19/">site web des réunions Debian</a>.
Presque toutes les sessions ont facilité la participation à distance par
des applications de messagerie IRC ou un éditeur de texte collaboratif
en ligne.
</p>

<p>
Le site web de <a href="https://debconf19.debconf.org/">DebConf19</a>
restera actif à fin d'archive et continuera à offrir des liens vers les
présentations et vidéos des communications et des événements.
</p>

<p>
L'an prochain, <a href="https://wiki.debian.org/DebConf/20">DebConf20</a> se tiendra
à Haïfa, Israël, du 23 au 29 août 2020. Comme à l'accoutumée, les
organisateurs débuteront les travaux en Israël, avant la DebConf, par un
DebCamp (du 16 au 22 août), avec un accent particulier mis sur le travail
individuel ou en équipe pour améliorer la distribution.
</p>

<p>
DebConf s'est engagée à offrir un environnement sûr et accueillant pour
tous les participants. Durant la conférence, plusieurs équipes (le
secrétariat, l'équipe d'accueil et l'équipe anti-harcèlement)
étaient disponibles pour offrir aux participants, aussi bien sur place
que distants, la meilleure expérience de la conférence, et trouver des
solutions à tout problème qui aurait pu subvenir.
Voir la <a href="https://debconf19.debconf.org/about/coc/">page web à propos
du Code de conduite sur le site de DebConf19</a>
pour plus de détails à ce sujet.
</p>

<p>
Debian remercie les nombreux <a href="https://debconf19.debconf.org/sponsors/">parrains</a>
pour leur engagement dans leur soutien à DebConf19, et en particulier nos
parrains platine
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://google.com/">Google</a>
et <a href="https://www.lenovo.com">Lenovo</a>.
</p>

<h2>À propos de Debian</h2>

<p>
Le projet Debian a été fondé en 1993 par Ian Murdock pour être un projet
communautaire réellement libre. Depuis cette date, le projet Debian est
devenu l'un des plus importants et des plus influents projets à code source
ouvert. Des milliers de volontaires du monde entier travaillent ensemble
pour créer et maintenir les logiciels Debian. Traduite en soixante-dix
langues et prenant en charge un grand nombre de types d'ordinateurs, la
distribution Debian est conçue pour être le <q>système d'exploitation universel</q>.
</p>

<h2>À propos de DebConf</h2>

<p>
DebConf est la conférence des développeurs du projet Debian. En plus
d'un programme complet de présentations techniques, sociales ou
organisationnelles, DebConf fournit aux développeurs, aux contributeurs et
à toutes personnes intéressées, une occasion de rencontre et de travail
collaboratif interactif. DebConf a eu lieu depuis 2000 en des endroits du
monde aussi variés que l'Écosse, l'Argentine ou la Bosnie-Herzégovine. Plus
d'information sur DebConf est disponible à l'adresse
<a href="https://debconf.org/">https://debconf.org</a>.
</p>

<h2>À propos d'Infomaniak</h2>

<p>
<a href="https://www.infomaniak.com">Infomaniak</a> est la plus grande compagnie
suisse d'hébergement web qui offre aussi des services de sauvegarde et de
stockage, des solutions pour les organisateurs d'événements et des
services de diffusion en direct et de vidéo à la demande.
Elle possède l'intégralité de ses centres de données et des éléments
critiques pour le fonctionnement des services et produits qu'elle fournit
(à la fois sur le plan matériel et logiciel).
</p>

<h2>À propos de Google</h2>

<p>
<a href="https://google.com/">Google</a> est l'une des plus grandes entreprises
technologiques du monde qui fournit une large gamme de services relatifs
à Internet et de produits tels que des technologies de publicité en
ligne, des outils de recherche, de l'informatique dans les nuages, des
logiciels et du matériel.

Google apporte son soutien à Debian en parrainant la DebConf depuis plus
de dix ans, et est également un partenaire de Debian en parrainant les
composants de l'infrastructure d'intégration continue de
<a href="https://salsa.debian.org">Salsa</a> au sein de la plateforme
Google Cloud.
</p>

<h2>À propos de Lenovo</h2>

<p>
En tant que leader mondial en technologie, produisant une vaste gamme de
produits connectés, comprenant des smartphones, des tablettes, des
machines de bureau et des stations de travail aussi bien que des
périphériques de réalité augmentée et de réalité virtuelle, des solutions
pour la domotique ou le bureau connecté et les centres de données,
<a href="https://www.lenovo.com">Lenovo</a> a compris combien étaient
essentiels les plateformes et systèmes ouverts pour un monde connecté.
</p>

<h2>Plus d'informations</h2>

<p>
Pour plus d'informations, veuillez consulter la page internet de
DebConf19 à l'adresse
<a href="https://debconf19.debconf.org/">https://debconf19.debconf.org/</a>
ou envoyez un message à &lt;press@debian.org&gt;.
</p>
