#use wml::debian::translation-check translation="42b6b660e613679ceb4726fb1a5aebaa47b68d96" mindelta="1"
<define-tag description>säkerhetsuppdatering</define-tag>
<define-tag moreinfo>
<p>Flera forskare har upptäckt sårbarheter i sättet som designen av
Intel-processorer har implementerat spekulativ vidarebefordring av data
fyllt i temporära mikroarkitekturella strukturer (buffrar). Denna
brist kunde tillåta en angripare som kontrollerar en icke-priviligierad
process att läsa känslig information, inklusive från kärnan och alla andra
processer som kör på systemet eller över gäst/värd-avgränsningar för att
läsa värdminne.</p>

<p>Se <a href="https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/mds.html">\
https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/mds.html</a>
för ytterligare information.</p>

<p>För att fullständigt behandla dessa sårbarheter är det även nödvändigt att
installera uppdaterad CPU-mikrokod. Ett uppdaterat intel-microcode-paket (som
endast finns tillgängligt i Debian non-free) kommer att tillhandahållas i en
separat DSA. Den uppdaterade CPU-mikrokoden kan även finnas tillgänglig som en
del av en system firmwar ("BIOS")-uppdatering.</p>

<p>Utöver detta innehåller denna uppdatering en rättning för en regression
som orsakar låsningar i loopbackdrivrutinen, som introducerades av uppdateringen
till 4.9.168 i den senaste Stretch-punktutgåvan.</p>

<p>För den stabila utgåvan (Stretch) har dessa problem rättats i
version 4.9.168-1+deb9u2.</p>

<p>Vi rekommenderar att ni uppgraderar era linux-paket.</p>

<p>För detaljerad säkerhetsstatus om linux vänligen se
dess säkerhetsspårare på
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4444.data"
