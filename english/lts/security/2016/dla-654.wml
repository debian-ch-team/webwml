<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that there was a integer overflow in libxfixes, a library
providing a client interface to the X11 <q>XFIXES</q> extension.</p>

<p>The 32 bit field <q>rep.length</q> was not checked for validity, which allowed an
integer overflow on 32 bit systems. A malicious server could send INT_MAX as
<q>length</q> which was then multiplied by the size of XRectangle. In this case the
client would not read the whole data from server, getting out of sync.</p>

<p>For Debian 7 <q>Wheezy</q>, this issue has been fixed in libxfixes version
1:5.0-4+deb7u2.</p>

<p>We recommend that you upgrade your libxfixes packages.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-654.data"
# $Id: $
