<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several security issues have been identified and corrected in ICU, the
International Components for Unicode C and C++ library, in Debian Wheezy.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-2632">CVE-2015-2632</a>

    <p>Buffer overflow vulnerability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-4844">CVE-2015-4844</a>

    <p>Buffer overflow vulnerability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-0494">CVE-2016-0494</a>

    <p>Integer signedness/overflow vulnerability.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.8.1.1-12+deb7u4.</p>

<p>We recommend that you upgrade your icu packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-545.data"
# $Id: $
