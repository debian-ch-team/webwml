<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>In Mercurial before 4.1.3, "hg serve --stdio" allows remote
authenticated users to launch the Python debugger, and consequently
execute arbitrary code, by using --debugger as a repository name.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.2.2-4+deb7u4.</p>

<p>We recommend that you upgrade your mercurial packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1005.data"
# $Id: $
