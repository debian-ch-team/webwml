<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Maor Shwartz, Jeremy Heng and Terry Chia discovered two security
vulnerabilities in Graphicsmagick, a collection of image processing tool
s.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16352">CVE-2017-16352</a>

   <p>Graphicsmagick was vulnerable to a heap-based buffer
   overflow vulnerability found in the <q>Display visual image directory</q>
   feature of the DescribeImage() function of the magick/describe.c
   file. One possible  way to trigger the vulnerability is to run the
   identify command on a specially crafted MIFF format file with the
   verbose flag.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16353">CVE-2017-16353</a>

   <p>Graphicsmagick was vulnerable to a memory information disclosure
   vulnerability found in the DescribeImage function of the
   magick/describe.c file, because of a heap-based buffer over-read. The
   portion of the code containing the vulnerability is responsible for
   printing the IPTC Profile information contained in the image. This
   vulnerability can be triggered with a specially crafted MIFF file.
   There is an out-of-bounds buffer dereference because certain
   increments are never checked.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.3.16-1.1+deb7u13.</p>

<p>We recommend that you upgrade your graphicsmagick packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1159.data"
# $Id: $
