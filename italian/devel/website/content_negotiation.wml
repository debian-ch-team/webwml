#use wml::debian::template title="Negoziazione dei contenuti"
#use wml::debian::translation-check translation="c646e774c01abc2ee3d32c65d6920ea4f03159dc" maintainer="skizzhg"

<h3>Come fa il server a sapere quale file utilizzare</h3>
<p>Avrai notato che i link interni non finiscono in .html. Questo
è dovuto al fatto che il server usa la negoziazione dei contenuti
per decidere quale versione del documento distribuire. Quando
ci sono più versioni del documento, il server fa un elenco di
alternative, cioè: se è richiesto il documento 'about' allora la
lista comprenderà about.en.html, about.de.html, ... . I server
Debian restituiscono in modo predefinito la versione inglese del
documento, ma questo è configurabile.
</p>

<p>Se un client ha la configurazione corretta, per esempio richiede
il tedesco, allora nell'esempio precedente verrà scelto il file
about.de.html. La cosa bella di questa configurabilità è che
se la lingua non è disponibile allora si può avere una pagina in
una lingua diversa (che è sempre meglio di niente.)
La scelta del documento da servire è un po' complessa, se sei
interessato ti rimandiamo quindi alla fonte originaria della spiegazione
<a href="https://httpd.apache.org/docs/current/content-negotiation.html">
https://httpd.apache.org/docs/current/content-negotiation.html</a>.
</p>

<p>Poiché molti utenti non sanno neppure cosa sia la negoziazione dei
contenuti, ci sono dei link al fondo di ogni pagina che rimandano
direttamente a tutte le altre lingue disponibili.
Questo elenco è creato da uno script perl chiamato da wml quando la pagina
viene generata.
</p>

<p>Esiste anche un'opzione che permette di ignorare le preferenze sulla
lingua del browser utlizzando un cookie che aumenta la priorità di una
particolare lingua rispetto alle preferenze del browser.
</p>
